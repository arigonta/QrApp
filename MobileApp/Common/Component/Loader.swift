//
//  Loader.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

public class MavSpinner: UIView {
    // MARK: - View builder
    private lazy var containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = spinnerContainerSize / 2
        view.backgroundColor = .clear
        return view
    }()
    
    private let spinnerView: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.color = .darkGray
        return activityIndicator
    }()
    
    // MARK: - Public properties
    public var spinnerContainerSize: CGFloat = 96
    
    // MARK: - Lifecycle
    public init(attachTo parent: UIView) {
        super.init(frame: parent.frame)
        componentSetup()
        attachWithAnimation(to: parent)
    }
    
    public init(attachTo parent: UIView, withoutDarkView: Bool) {
        super.init(frame: parent.frame)
        if withoutDarkView {
            componentSetupWithoutDarkView()
        } else {
            componentSetup()
        }
        attachWithAnimation(to: parent)
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public API
    public func dismiss() {
        if let parent = self.superview {
            UIView.transition(with: parent, duration: 0.3, options: .transitionCrossDissolve) { [weak self] in
                guard let self else { return }
                self.removeFromSuperview()
            }
        }
    }
    
    public func startLoader() {
        showLoader()
    }
    
    public func stopLoader() {
        spinnerView.stopAnimating()
    }
    
    // MARK: - Private functions
    private func componentSetup() {
        self.containerView.addSubview(self.spinnerView)
        self.addSubview(self.containerView)
        self.setComponentConstraints()
        showLoader()
        self.containerView.layer.zPosition = self.layer.zPosition + 2
    }
    
    private func componentSetupWithoutDarkView() {
        self.containerView.addSubview(self.spinnerView)
        self.addSubview(self.containerView)
        self.setComponentConstraints()
        showLoader()
    }
    
    private func setComponentConstraints() {
        translatesAutoresizingMaskIntoConstraints = false
        spinnerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            spinnerView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            spinnerView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            
            containerView.heightAnchor.constraint(equalToConstant: spinnerContainerSize),
            containerView.widthAnchor.constraint(equalTo: containerView.heightAnchor),
            
            containerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            containerView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    private func attachWithAnimation(to parent: UIView) {
        UIView.transition(with: parent, duration: 0.3, options: .transitionCrossDissolve) { [weak self] in
            guard let self else { return }
            parent.addSubview(self)
            setFullScreen()
        }
    }
    
    private func setFullScreen() {
        guard let parent = superview else { return }
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: parent.topAnchor),
            bottomAnchor.constraint(equalTo: parent.bottomAnchor),
            leadingAnchor.constraint(equalTo: parent.leadingAnchor),
            trailingAnchor.constraint(equalTo: parent.trailingAnchor)
        ])
    }
    
    private func showLoader() {
        spinnerView.startAnimating()
    }
}
