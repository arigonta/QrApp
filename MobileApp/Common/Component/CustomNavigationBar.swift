//
//  CustomNavigationBar.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

class CustomNavigationBar: UIView {
    // Back button closure
    var backButtonAction: (() -> Void)?

    // Title label
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .black // Customize title color as needed
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold) // Customize font as needed
        return label
    }()

    // Back button
    private let backButton: UIButton = {
        let button = UIButton()
        let backImage = UIImage(systemName: "chevron.left") // Customize back button image as needed
        button.setImage(backImage, for: .normal)
        button.tintColor = .black // Customize back button color as needed
        button.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }

    private func setupUI() {
        // Add title label
        addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])

        // Add back button
        addSubview(backButton)
        backButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            backButton.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
            backButton.widthAnchor.constraint(equalToConstant: 30),
            backButton.heightAnchor.constraint(equalToConstant: 30)
        ])
    }

    // Action when back button is tapped
    @objc private func backButtonTapped() {
        backButtonAction?()
    }

    // Set title text
    func setTitle(_ title: String) {
        titleLabel.text = title
    }
}
