//
//  AppDelegate.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Check if the app is running in a multi-scene environment
        if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene {
            // Create a UIWindow using the windowScene constructor
            let window = UIWindow(windowScene: windowScene)
            
            // Create an instance of MobileAppConfigurator
            let configurator = MobileAppConfigurator.shared
            
            // Get the root view controller from createLoginModule
            let rootViewController = configurator.createHomeModule()
            
            window.frame = UIScreen.main.bounds

            // Set the root view controller of the window
            window.rootViewController = rootViewController
            
            if #available(iOS 13.0, *) {
                window.overrideUserInterfaceStyle = .light
            }
            
            // Set the window of the AppDelegate
            self.window = window
            
            // Make the window visible
            window.makeKeyAndVisible()
        }
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

