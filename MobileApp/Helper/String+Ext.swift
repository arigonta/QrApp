//
//  String+Ext.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

extension String {
    func formattedCurrency() -> String? {
        // Convert the string to an integer
        if let amount = Int(self) {
            // Create a NumberFormatter instance
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = Locale(identifier: "id_ID") // Set locale to Indonesian
            
            // Format the number
            if let formattedAmount = formatter.string(from: NSNumber(value: amount)) {
                return formattedAmount
            }
        }
        return nil
    }
    
    var toDecimal: Decimal? {
        return Decimal(string: self)
    }
}
