//
//  Formatter+Ext.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import Foundation

extension Formatter {
    static let stringFormatters: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .none
        return formatter
    }()
}
