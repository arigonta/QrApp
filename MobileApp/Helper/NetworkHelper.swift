//
//  NetworkHelper.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    // Add other HTTP methods as needed
}

class NetworkHelper {
    static let shared = NetworkHelper()

    private init() {}

    func request<T: Codable>(urlString: String, method: HTTPMethod, parameters: [String: Any]?, headers: [String: String]?, completion: @escaping (Result<T, Error>) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(.failure(NetworkError.invalidURL))
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue

        // Set request headers
        headers?.forEach { key, value in
            request.addValue(value, forHTTPHeaderField: key)
        }

        // Set request body for POST requests
        if method == .post {
            if let parameters = parameters {
                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: parameters)
                } catch {
                    completion(.failure(NetworkError.invalidParameters))
                    return
                }
            }
        }

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async { // Ensure execution on main thread
                if let error = error {
                    completion(.failure(error))
                    return
                }

                guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
                    completion(.failure(NetworkError.invalidResponse))
                    return
                }

                guard let data = data else {
                    completion(.failure(NetworkError.noData))
                    return
                }

                do {
                    // Decode the response data into the generic model type T
                    let decodedData = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(decodedData))
                } catch {
                    completion(.failure(error))
                }
            }
        }

        task.resume()
    }
}

enum NetworkError: Error {
    case invalidURL
    case invalidParameters
    case invalidResponse
    case noData
    // Add other network-related errors as needed
}
