//
//  Decimal+Ext.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import Foundation

extension Decimal {
    var stringValue: String {
        return Formatter.stringFormatters.string(for: self) ?? ""
    }
}
