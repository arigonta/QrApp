//
//  QrPaymentResultView.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

class QrPaymentResultView: UIViewController, QrPaymentResultPresenterToView {
    var presenter: QrPaymentResultViewToPresenter?
    
    @IBOutlet weak var currentBalanceLabel: UILabel!
    @IBOutlet weak var trxHistoryButton: UIButton!
    
    init() {
        super.init(nibName: String(describing: QrPaymentResultView.self), bundle: Bundle(for: QrPaymentResultView.self))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    func setupView(currentBalance: String) {
        currentBalanceLabel.text = currentBalance.formattedCurrency()
        
        trxHistoryButton.backgroundColor = .blue
        trxHistoryButton.layer.cornerRadius = 12

        trxHistoryButton.addAction { [weak self] in
            guard let self else { return }
            presenter?.didTapTrxHistory()
        }
    }
}
