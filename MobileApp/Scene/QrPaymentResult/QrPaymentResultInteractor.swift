// 
//  QrPaymentResultInteractor.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import Foundation

class QrPaymentResultInteractor: QrPaymentResultPresenterToInteractor {
    weak var presenter: QrPaymentResultInteractorToPresenter?
        
    func getCurrentBalance(qrModel: QrViewModel.QrViewModel) -> String {
        // Check if both nominal and nominalPayment are not nil
        guard let nominalString = qrModel.nominal,
              let nominalPaymentString = qrModel.nominalPayment,
              let nominal = nominalString.toDecimal,
              let nominalPayment = nominalPaymentString.toDecimal else {
            return "Error: Invalid input"
        }
        
        // Calculate the current balance
        let currentBalance = nominal - nominalPayment
        
        // Convert the current balance to a string
        let currentBalanceString = currentBalance.stringValue
        
        return currentBalanceString
    }
    
    func saveQrTrxHistoryToUserDefaults(qrViewModel: [QrViewModel.QrTransactionHistory]) {
        let encoder = JSONEncoder()
        if let encodedData = try? encoder.encode(qrViewModel) {
            UserDefaults.standard.set(encodedData, forKey: "qrTrxHistory")
        }
    }
}
