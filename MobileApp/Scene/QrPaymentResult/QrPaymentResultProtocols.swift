// 
//  QrPaymentResultProtocols.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

// MARK: View -
protocol QrPaymentResultPresenterToView: AnyObject {
    var presenter: QrPaymentResultViewToPresenter? { get set }
    
    func setupView(currentBalance: String)
}

// MARK: Interactor -
protocol QrPaymentResultPresenterToInteractor: AnyObject {
    var presenter: QrPaymentResultInteractorToPresenter? { get set }
    
    func getCurrentBalance(qrModel: QrViewModel.QrViewModel) -> String
    func saveQrTrxHistoryToUserDefaults(qrViewModel: [QrViewModel.QrTransactionHistory])
}

// MARK: Router -
protocol QrPaymentResultPresenterToRouter: AnyObject {
    func navigateToQrTrxHistory(view: QrPaymentResultPresenterToView?)
}

// MARK: Presenter -
protocol QrPaymentResultViewToPresenter: AnyObject {
    var view: QrPaymentResultPresenterToView? { get set }
    var interactor: QrPaymentResultPresenterToInteractor? { get set }
    var router: QrPaymentResultPresenterToRouter? { get set }
    var context: QrViewModel.QrViewModel { get set }

    func didTapTrxHistory()
    func viewDidLoad()
}

protocol QrPaymentResultInteractorToPresenter: AnyObject {
}
