// 
//  QrPaymentResultRouter.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

class QrPaymentResultRouter: QrPaymentResultPresenterToRouter {
    func navigateToQrTrxHistory(view: QrPaymentResultPresenterToView?) {
        let qrScannerVC = MobileAppConfigurator.shared.createQrTransactionHistoryModule()
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(qrScannerVC, animated: true)
        }
    }
}
