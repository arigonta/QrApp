//
//  QrPaymentResultPresenter.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import Foundation

class QrPaymentResultPresenter: QrPaymentResultViewToPresenter {
    weak var view: QrPaymentResultPresenterToView?
    var interactor: QrPaymentResultPresenterToInteractor?
    var router: QrPaymentResultPresenterToRouter?
    
    var context = QrViewModel.QrViewModel()
    var qrViewModel = QrViewModel()

    func didTapTrxHistory() {
        // Create a new QrTransactionHistory object using the current context
        let historyItem = QrViewModel.QrTransactionHistory(merchantName: context.merchant,
                                                               paymentNominal: context.nominalPayment,
                                                               currentBalance: interactor?.getCurrentBalance(qrModel: context),
                                                               timeStamp: getCurrentTimeStamp())
        
        // Append the history item to qrTrxHistory
        qrViewModel.qrTrxHistory.append(historyItem)
        
        // Save qrTrxHistory to UserDefaults
        interactor?.saveQrTrxHistoryToUserDefaults(qrViewModel: qrViewModel.qrTrxHistory)
        
        router?.navigateToQrTrxHistory(view: view)
    }
    
    func viewDidLoad() {
        view?.setupView(currentBalance: interactor?.getCurrentBalance(qrModel: context) ?? "")
    }
    
    private func getCurrentTimeStamp() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: Date())
    }
}


extension QrPaymentResultPresenter: QrPaymentResultInteractorToPresenter {}
