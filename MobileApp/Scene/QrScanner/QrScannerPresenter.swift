//
//  QrScannerPresenter.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import Foundation

class QrScannerPresenter: QrScannerViewToPresenter {
    weak var view: QrScannerPresenterToView?
    var interactor: QrScannerPresenterToInteractor?
    var router: QrScannerPresenterToRouter?
    
    var context = QrViewModel.QrViewModel()
    
    func didScanQr(value: String) {
        // Parse the QR code value and update the presenter's context
        guard let qrModel = parseQRCode(value) else {
            // Failed to parse QR code, handle error or notify view
            print("Failed to parse QR code: \(value)")
            return
        }
        
        // Update the presenter's context with the parsed QR model
        context.nominalPayment = qrModel.nominalPayment
        context.sof = qrModel.sof
        context.merchant = qrModel.merchant
        context.transactionId = qrModel.transactionId
        
        view?.setupQrResult(nominal: context.nominalPayment ?? "",
                            trxId: context.transactionId ?? "",
                            merchant: context.merchant ?? "",
                            sof: context.sof ?? "")
    }
    
    func didTapPay() {
        router?.navigateToQrPaymentResult(view: view, context: context)
    }

    private func parseQRCode(_ qrCode: String) -> QrViewModel.QrViewModel? {
        let components = qrCode.components(separatedBy: ".")
        // Extract values from components
        let urlComponents = components[0].components(separatedBy: "/")
        guard let sof = urlComponents.last else {
            return nil
        }
        let trxId = components[1]
        let merchant = components[2]
        let nominal = components[3]
        
        // Create and return a QrViewModel object with the extracted values
        return QrViewModel.QrViewModel(
            name: nil,
            nominalPayment: nominal,
            nominal: nil,
            sof: sof,
            merchant: merchant,
            transactionId: trxId
        )
    }
    
    func navigateBack() {
        router?.navigateBack(from: view)
    }
}

extension QrScannerPresenter: QrScannerInteractorToPresenter {}
