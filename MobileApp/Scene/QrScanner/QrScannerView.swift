//
//  QrScannerView.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit
import AVFoundation

class QrScannerView: UIViewController, QrScannerPresenterToView {
    var presenter: QrScannerViewToPresenter?
    @IBOutlet weak var navBarContainerView: UIView!
    @IBOutlet weak var qrScanner: UIView!
    private let customNavBar = CustomNavigationBar()
    var lastProcessedQRCode: String?

    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var nominalLabel: UILabel!
    @IBOutlet weak var sofLabel: UILabel!
    @IBOutlet weak var idTrxLabel: UILabel!
    @IBOutlet weak var merchantLabel: UILabel!
    @IBOutlet weak var qrResult: UIView!
    
    // AVFoundation variables
    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var spinner: MavSpinner?

    override func viewDidLoad() {
        super.viewDidLoad()
        qrResult.isHidden = true
        setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            guard let self = self else { return }
            self.setupQRScanner()
        }
    }
    
    func setupQrResult(nominal: String,
                       trxId: String,
                       merchant: String,
                       sof: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            guard let self else { return }
            customNavBar.setTitle("QR Confirmation")
            qrResult.isHidden = false
            stopQRScanner()
            
            payButton.backgroundColor = .blue
            payButton.layer.cornerRadius = 12
            
            nominalLabel.text = nominal.formattedCurrency()
            idTrxLabel.text = trxId
            merchantLabel.text = merchant
            sofLabel.text = sof
        }
        
        payButton.addAction { [weak self] in
            guard let self else { return }
            presenter?.didTapPay()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    private func setupNavigationBar() {
        // Add custom navigation bar to navBarContainerView
        navBarContainerView.addSubview(customNavBar)
        customNavBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            customNavBar.leadingAnchor.constraint(equalTo: navBarContainerView.leadingAnchor),
            customNavBar.trailingAnchor.constraint(equalTo: navBarContainerView.trailingAnchor),
            customNavBar.topAnchor.constraint(equalTo: navBarContainerView.topAnchor),
            customNavBar.bottomAnchor.constraint(equalTo: navBarContainerView.bottomAnchor)
        ])
        
        // Set title
        customNavBar.setTitle("QR Scanner")
        
        // Assign back button action
        customNavBar.backButtonAction = { [weak self] in
            self?.presenter?.navigateBack()
        }
    }
    
    private func setupQRScanner() {
        captureSession = AVCaptureSession()
        lastProcessedQRCode = nil
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            print("Failed to get video capture device")
            return
        }

        do {
            let videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
            captureSession?.addInput(videoInput)
        } catch {
            print("Failed to add video input")
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()
        if let session = captureSession, session.canAddOutput(metadataOutput) {
            session.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            print("Failed to add metadata output")
            return
        }

        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession ?? AVCaptureSession())
        videoPreviewLayer?.videoGravity = .resizeAspectFill
        
        // Adjust the frame of the videoPreviewLayer to fit the bounds of qrScanner view
        videoPreviewLayer?.frame = qrScanner.bounds
                
        // Add the videoPreviewLayer to the qrScanner view's layer
        if let viewPreview = videoPreviewLayer {
            qrScanner.layer.addSublayer(viewPreview)
        }
        
        startQRScanner()
    }
    
    private func startQRScanner() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            if !(captureSession?.isRunning ?? false) {
                captureSession?.startRunning()
            }
        }
    }
    
    private func stopQRScanner() {
        if captureSession?.isRunning ?? false {
            captureSession?.stopRunning()
        }
    }
    
    private func handleQRCode(_ qrCode: String) {
        // Parse and handle the QR code data
        presenter?.didScanQr(value: qrCode)
    }
}

extension QrScannerView: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        guard let metadataObject = metadataObjects.first as? AVMetadataMachineReadableCodeObject else {
            return
        }
            
        if metadataObject.type == .qr, let qrCode = metadataObject.stringValue, qrCode != lastProcessedQRCode {
            lastProcessedQRCode = qrCode
            // Process the QR code
            handleQRCode(qrCode)
        }
    }
}
