// 
//  QrScannerProtocols.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

// MARK: View -
protocol QrScannerPresenterToView: AnyObject {
    var presenter: QrScannerViewToPresenter? { get set }
    
    func setupQrResult(nominal: String,
                       trxId: String,
                       merchant: String,
                       sof: String)
}

// MARK: Interactor -
protocol QrScannerPresenterToInteractor: AnyObject {
    var presenter: QrScannerInteractorToPresenter? { get set }
}

// MARK: Router -
protocol QrScannerPresenterToRouter: AnyObject {
    func navigateBack(from view: QrScannerPresenterToView?)
    func navigateToQrPaymentResult(view: QrScannerPresenterToView?, context: QrViewModel.QrViewModel)
}

// MARK: Presenter -
protocol QrScannerViewToPresenter: AnyObject {
    var view: QrScannerPresenterToView? { get set }
    var interactor: QrScannerPresenterToInteractor? { get set }
    var router: QrScannerPresenterToRouter? { get set }
    var context: QrViewModel.QrViewModel { get set }
    
    func navigateBack()
    func didScanQr(value: String)
    func didTapPay()
}

protocol QrScannerInteractorToPresenter: AnyObject {
}
