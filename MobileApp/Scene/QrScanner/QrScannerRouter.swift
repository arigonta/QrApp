// 
//  QrScannerRouter.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

class QrScannerRouter: QrScannerPresenterToRouter {
    func navigateBack(from view: QrScannerPresenterToView?) {
        guard let viewC = view as? UIViewController else { return }
        viewC.navigationController?.popViewController(animated: true)
    }
    
    func navigateToQrPaymentResult(view: QrScannerPresenterToView?, context: QrViewModel.QrViewModel) {
        let qrScannerVC = MobileAppConfigurator.shared.createQrPaymentResultModule(context: context)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(qrScannerVC, animated: true)
        }
    }
}
