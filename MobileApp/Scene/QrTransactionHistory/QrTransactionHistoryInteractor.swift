// 
//  QrTransactionHistoryInteractor.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import Foundation

class QrTransactionHistoryInteractor: QrTransactionHistoryPresenterToInteractor {
    weak var presenter: QrTransactionHistoryInteractorToPresenter?
        
    func retrieveTransactionHistory(from key: String) {
        // Retrieve transaction history from UserDefaults using the provided key
        if let encodedData = UserDefaults.standard.data(forKey: key),
           let history = try? JSONDecoder().decode([QrViewModel.QrTransactionHistory].self, from: encodedData) {
            presenter?.transactionHistoryRetrieved(history: history)
        } else {
            // Notify presenter if transaction history retrieval fails
            presenter?.transactionHistoryRetrievalFailed()
        }
    }
}
