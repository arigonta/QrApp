//
//  QrTransactionHistoryView.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

class QrTransactionHistoryView: UIViewController, QrTransactionHistoryPresenterToView {
    var presenter: QrTransactionHistoryViewToPresenter?
    
    private let customNavBar = CustomNavigationBar()
    private let scrollView = UIScrollView()
    private let stackView = UIStackView()
    @IBOutlet weak var navigationBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupScrollView()
        setupStackView()
        presenter?.retrieveTransactionHistory()
    }
    
    private func setupNavigationBar() {
        // Add custom navigation bar to navigationBar
        navigationBar.addSubview(customNavBar)
        customNavBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            customNavBar.leadingAnchor.constraint(equalTo: navigationBar.leadingAnchor),
            customNavBar.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor),
            customNavBar.topAnchor.constraint(equalTo: navigationBar.topAnchor),
            customNavBar.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor)
        ])
        
        // Set title
        customNavBar.setTitle("Transaction History")
        
        // Assign back button action
        customNavBar.backButtonAction = { [weak self] in
            self?.presenter?.navigateBack()
        }
    }
    
    private func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: navigationBar.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func setupStackView() {
        scrollView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 20),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -20),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -40)
        ])
    }
    
    func displayTransactionHistory(_ history: [QrViewModel.QrTransactionHistory]) {
        // Remove existing subviews from stack view
        stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        // Iterate through the transaction history and display it in the UI
        for transaction in history {
            let merchantLabel = UILabel()
            merchantLabel.text = "Merchant: \(transaction.merchantName ?? "")"
            
            let paymentLabel = UILabel()
            paymentLabel.text = "Payment: \(transaction.paymentNominal ?? "")"
            
            let balanceLabel = UILabel()
            balanceLabel.text = "Balance: \(transaction.currentBalance ?? "")"
            
            let timeStampLabel = UILabel()
            timeStampLabel.text = "Time Stamp: \(transaction.timeStamp ?? "")"
            
            let separatorView = UIView()
            separatorView.backgroundColor = .lightGray
            separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
            
            stackView.addArrangedSubview(merchantLabel)
            stackView.addArrangedSubview(paymentLabel)
            stackView.addArrangedSubview(balanceLabel)
            stackView.addArrangedSubview(timeStampLabel)
            stackView.addArrangedSubview(separatorView)
        }
    }
    
    func displayNoTransactionHistoryMessage() {
        let noHistoryLabel = UILabel()
        noHistoryLabel.text = "No transaction history available"
        stackView.addArrangedSubview(noHistoryLabel)
    }
}
