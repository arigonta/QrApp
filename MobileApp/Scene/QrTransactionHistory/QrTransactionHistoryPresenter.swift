//
//  QrTransactionHistoryPresenter.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import Foundation

class QrTransactionHistoryPresenter: QrTransactionHistoryViewToPresenter {
    weak var view: QrTransactionHistoryPresenterToView?
    var interactor: QrTransactionHistoryPresenterToInteractor?
    var router: QrTransactionHistoryPresenterToRouter?
    
    func navigateBack() {
        router?.navigateBack(from: view)
    }
    
    func retrieveTransactionHistory() {
        interactor?.retrieveTransactionHistory(from: "qrTrxHistory")
    }
}

extension QrTransactionHistoryPresenter: QrTransactionHistoryInteractorToPresenter {
    func transactionHistoryRetrieved(history: [QrViewModel.QrTransactionHistory]) {
        // Call view method to display the retrieved transaction history
        view?.displayTransactionHistory(history)
    }
    
    func transactionHistoryRetrievalFailed() {
        // Handle failure to retrieve transaction history
        view?.displayNoTransactionHistoryMessage()
    }
}
