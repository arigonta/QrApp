// 
//  QrTransactionHistoryProtocols.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

// MARK: View -
protocol QrTransactionHistoryPresenterToView: AnyObject {
    var presenter: QrTransactionHistoryViewToPresenter? { get set }
    
    func displayTransactionHistory(_ history: [QrViewModel.QrTransactionHistory])
    func displayNoTransactionHistoryMessage()
}

// MARK: Interactor -
protocol QrTransactionHistoryPresenterToInteractor: AnyObject {
    var presenter: QrTransactionHistoryInteractorToPresenter? { get set }
    func retrieveTransactionHistory(from key: String)
}

// MARK: Router -
protocol QrTransactionHistoryPresenterToRouter: AnyObject {
    func navigateBack(from view: QrTransactionHistoryPresenterToView?)
}

// MARK: Presenter -
protocol QrTransactionHistoryViewToPresenter: AnyObject {
    var view: QrTransactionHistoryPresenterToView? { get set }
    var interactor: QrTransactionHistoryPresenterToInteractor? { get set }
    var router: QrTransactionHistoryPresenterToRouter? { get set }
    
    func retrieveTransactionHistory()
    func navigateBack()
}

protocol QrTransactionHistoryInteractorToPresenter: AnyObject {
    func transactionHistoryRetrievalFailed()
    func transactionHistoryRetrieved(history: [QrViewModel.QrTransactionHistory])
}
