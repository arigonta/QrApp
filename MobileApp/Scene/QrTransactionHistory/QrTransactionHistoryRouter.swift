// 
//  QrTransactionHistoryRouter.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

class QrTransactionHistoryRouter: QrTransactionHistoryPresenterToRouter {
    func navigateBack(from view: QrTransactionHistoryPresenterToView?) {
        guard let viewController = view as? UIViewController else { return }
        if let navigationController = viewController.navigationController {
            navigationController.popToRootViewController(animated: true)
        } else {
            viewController.dismiss(animated: true, completion: nil)
        }
    }
}
