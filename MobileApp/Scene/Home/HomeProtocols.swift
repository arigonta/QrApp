// 
//  HomeProtocols.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

// MARK: View -
protocol HomePresenterToView: AnyObject {
    var presenter: HomeViewToPresenter? { get set }
}

// MARK: Interactor -
protocol HomePresenterToInteractor: AnyObject {
    var presenter: HomeInteractorToPresenter? { get set }
}

// MARK: Router -
protocol HomePresenterToRouter: AnyObject {
    func navigateToQris(view: HomePresenterToView?)
}

// MARK: Presenter -
protocol HomeViewToPresenter: AnyObject {
    var view: HomePresenterToView? { get set }
    var interactor: HomePresenterToInteractor? { get set }
    var router: HomePresenterToRouter? { get set }
    
    func didTapQris()
}

protocol HomeInteractorToPresenter: AnyObject {
}
