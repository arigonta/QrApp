// 
//  HomeRouter.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

class HomeRouter: HomePresenterToRouter {
    func navigateToQris(view: HomePresenterToView?) {
        let qrLandingVC = MobileAppConfigurator.shared.createQrLandingModule(context: QrViewModel.QrViewModel())
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(qrLandingVC, animated: true)
        }
    }
}
