//
//  HomePresenter.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import Foundation

class HomePresenter: HomeViewToPresenter {
    weak var view: HomePresenterToView?
    var interactor: HomePresenterToInteractor?
    var router: HomePresenterToRouter?
    
    func didTapQris() {
        router?.navigateToQris(view: view)
    }
}

extension HomePresenter: HomeInteractorToPresenter {
}
