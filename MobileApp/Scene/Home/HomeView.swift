//
//  HomeView.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

class HomeView: UIViewController, HomePresenterToView {
    var presenter: HomeViewToPresenter?
    
    @IBOutlet weak var qrImage: UIImageView!
    
    init() {
        super.init(nibName: String(describing: HomeView.self), bundle: Bundle(for: HomeView.self))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setupView() {
        qrImage.addAction { [weak self] in
            guard let self else { return }
            presenter?.didTapQris()
        }
    }
}
