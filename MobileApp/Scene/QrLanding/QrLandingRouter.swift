// 
//  QrLandingRouter.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

class QrLandingRouter: QrLandingPresenterToRouter {
    func navigateBack(from view: QrLandingPresenterToView?) {
        guard let viewC = view as? UIViewController else { return }
        viewC.navigationController?.popViewController(animated: true)
    }
    
    func navigateToQrScanner(view: QrLandingPresenterToView?, context: QrViewModel.QrViewModel) {
        let qrScannerVC = MobileAppConfigurator.shared.createQrScannerModule(context: context)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(qrScannerVC, animated: true)
        }
    }
}
