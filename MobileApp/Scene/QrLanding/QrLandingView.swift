//
//  QrLandingView.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

class QrLandingView: UIViewController, QrLandingPresenterToView {
    var presenter: QrLandingViewToPresenter?
    @IBOutlet weak var navigationBar: UIView!
    
    @IBOutlet weak var qrisView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var qrisLabel: UILabel!
    
    private let customNavBar = CustomNavigationBar()

    init() {
        super.init(nibName: String(describing: QrLandingView.self), bundle: Bundle(for: QrLandingView.self))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        presenter?.viewDidLoad()
    }
    
    func setupView(name: String, value: String) {
        nameLabel.text = name
        valueLabel.text = value
        qrisLabel.text = "Navigate To Qris Scanner"
        qrisLabel.textColor = .blue
        
        qrisView.addAction { [weak self] in
            guard let self else { return }
            presenter?.didTapQris()
        }
    }
    
    private func setupNavigationBar() {
        // Add custom navigation bar to navBarContainerView
        navigationBar.addSubview(customNavBar)
        customNavBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            customNavBar.leadingAnchor.constraint(equalTo: navigationBar.leadingAnchor),
            customNavBar.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor),
            customNavBar.topAnchor.constraint(equalTo: navigationBar.topAnchor),
            customNavBar.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor)
        ])
        
        // Set title
        customNavBar.setTitle("QR Landing")
        
        // Assign back button action
        customNavBar.backButtonAction = { [weak self] in
            self?.presenter?.navigateBack()
        }
    }
}
