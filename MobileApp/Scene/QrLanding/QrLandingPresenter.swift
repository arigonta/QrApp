//
//  QrLandingPresenter.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import Foundation

class QrLandingPresenter: QrLandingViewToPresenter {
    weak var view: QrLandingPresenterToView?
    var interactor: QrLandingPresenterToInteractor?
    var router: QrLandingPresenterToRouter?
    
    var context = QrViewModel.QrViewModel()
    
    func viewDidLoad() {
        context = getModel()
        view?.setupView(name: "Hi! " + (context.name ?? ""),
                        value: context.nominal?.formattedCurrency() ?? "")
    }
    
    func getModel() -> QrViewModel.QrViewModel {
        var tempQrModel = QrViewModel.QrViewModel()
        tempQrModel.name = "Gonta"
        tempQrModel.nominal = "400000"
        return tempQrModel
    }
    
    func didTapQris() {
        router?.navigateToQrScanner(view: view, context: context)
    }
}

extension QrLandingPresenter: QrLandingInteractorToPresenter {
    func navigateBack() {
        router?.navigateBack(from: view)
    }
}
