// 
//  QrLandingProtocols.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

// MARK: View -
protocol QrLandingPresenterToView: AnyObject {
    var presenter: QrLandingViewToPresenter? { get set }
    
    func setupView(name: String, value: String)
}

// MARK: Interactor -
protocol QrLandingPresenterToInteractor: AnyObject {
    var presenter: QrLandingInteractorToPresenter? { get set }
}

// MARK: Router -
protocol QrLandingPresenterToRouter: AnyObject {
    func navigateBack(from view: QrLandingPresenterToView?)
    
    func navigateToQrScanner(view: QrLandingPresenterToView?, context: QrViewModel.QrViewModel)
}

// MARK: Presenter -
protocol QrLandingViewToPresenter: AnyObject {
    var view: QrLandingPresenterToView? { get set }
    var interactor: QrLandingPresenterToInteractor? { get set }
    var router: QrLandingPresenterToRouter? { get set }
    var context: QrViewModel.QrViewModel { get set }
    
    func navigateBack()
    func viewDidLoad()
    func didTapQris()
}

protocol QrLandingInteractorToPresenter: AnyObject {
}
