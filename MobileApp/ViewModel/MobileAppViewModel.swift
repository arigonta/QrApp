//
//  MobileAppViewModel.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import Foundation

public class QrViewModel {
    var qrTrxHistory: [QrTransactionHistory] = []
    
    struct QrViewModel {
        var name: String?
        var nominal: String?
        var nominalPayment: String?
        var sof: String?
        var merchant: String?
        var transactionId: String?
        
        init(name: String? = nil, 
             nominalPayment: String? = nil,
             nominal: String? = nil,
             sof: String? = nil,
             merchant: String? = nil,
             transactionId: String? = nil) {
            self.name = name
            self.nominalPayment = nominalPayment
            self.nominal = nominal
            self.sof = sof
            self.merchant = merchant
            self.transactionId = transactionId
        }
    }
    
    struct QrTransactionHistory: Codable {
        var merchantName: String?
        var paymentNominal: String?
        var currentBalance: String?
        var timeStamp: String?
        
        init(merchantName: String? = nil, paymentNominal: String? = nil, currentBalance: String? = nil, timeStamp: String? = nil) {
            self.merchantName = merchantName
            self.paymentNominal = paymentNominal
            self.currentBalance = currentBalance
            self.timeStamp = timeStamp
        }
    }
}

