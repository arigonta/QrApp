//
//  MobileAppConfigurator.swift
//  MobileApp
//
//  Created by Armadi Gonta on 23/02/24.
//

import UIKit

class MobileAppConfigurator {
    static let shared = MobileAppConfigurator()
    func createQrLandingModule(context: QrViewModel.QrViewModel) -> UIViewController {
        let view: UIViewController & QrLandingPresenterToView = QrLandingView()
        let presenter: QrLandingViewToPresenter & QrLandingInteractorToPresenter = QrLandingPresenter()
        let interactor: QrLandingPresenterToInteractor = QrLandingInteractor()
        let router: QrLandingPresenterToRouter = QrLandingRouter()
       
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        presenter.context = context
        interactor.presenter = presenter
       
        return view
    }

    func createHomeModule() -> UIViewController {
         let view: UIViewController & HomePresenterToView = HomeView()
         let presenter: HomeViewToPresenter & HomeInteractorToPresenter = HomePresenter()
         let interactor: HomePresenterToInteractor = HomeInteractor()
         let router: HomePresenterToRouter = HomeRouter()
        
         view.presenter = presenter
         presenter.view = view
         presenter.router = router
         presenter.interactor = interactor
         interactor.presenter = presenter
        
         return view
     }
    
    func createQrScannerModule(context: QrViewModel.QrViewModel) -> UIViewController {
        let view: UIViewController & QrScannerPresenterToView = QrScannerView()
        let presenter: QrScannerViewToPresenter & QrScannerInteractorToPresenter = QrScannerPresenter()
        let interactor: QrScannerPresenterToInteractor = QrScannerInteractor()
        let router: QrScannerPresenterToRouter = QrScannerRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        presenter.context = context
        
        return view
    }
    
    func createQrPaymentResultModule(context: QrViewModel.QrViewModel) -> UIViewController {
        let view: UIViewController & QrPaymentResultPresenterToView = QrPaymentResultView()
        let presenter: QrPaymentResultViewToPresenter & QrPaymentResultInteractorToPresenter = QrPaymentResultPresenter()
        let interactor: QrPaymentResultPresenterToInteractor = QrPaymentResultInteractor()
        let router: QrPaymentResultPresenterToRouter = QrPaymentResultRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        presenter.context = context
        
        return view
    }
    
    func createQrTransactionHistoryModule() -> UIViewController {
        let view: UIViewController & QrTransactionHistoryPresenterToView = QrTransactionHistoryView()
        let presenter: QrTransactionHistoryViewToPresenter & QrTransactionHistoryInteractorToPresenter = QrTransactionHistoryPresenter()
        let interactor: QrTransactionHistoryPresenterToInteractor = QrTransactionHistoryInteractor()
        let router: QrTransactionHistoryPresenterToRouter = QrTransactionHistoryRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
}
