//
//  QrScannerPresenterTests.swift
//  MobileAppTests
//
//  Created by Armadi Gonta on 23/02/24.
//

import XCTest
@testable import MobileApp

class QrScannerPresenterTests: XCTestCase {

    var presenter: QrScannerPresenter!
    var view: MockQrScannerView!
    var interactor: MockQrScannerInteractor!
    var router: MockQrScannerRouter!

    override func setUpWithError() throws {
        view = MockQrScannerView()
        interactor = MockQrScannerInteractor()
        router = MockQrScannerRouter()
        presenter = QrScannerPresenter()
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
    }

    override func tearDownWithError() throws {
        view = nil
        interactor = nil
        router = nil
        presenter = nil
    }

    func testDidScanQr() {
        let qrCodeValue = "http://BNI.ID12345678.MERCHANT MOCK TEST.50000"
        presenter.didScanQr(value: qrCodeValue)
        
        // Check if the view's setupQrResult method is called with the correct values
        XCTAssertEqual(view.setupQrResultCalledWithNominal, "50000", "View's setupQrResult method should be called with correct nominal value")
        XCTAssertEqual(view.setupQrResultCalledWithTrxId, "ID12345678", "View's setupQrResult method should be called with correct transaction ID")
        XCTAssertEqual(view.setupQrResultCalledWithMerchant, "MERCHANT MOCK TEST", "View's setupQrResult method should be called with correct merchant value")
    }

    func testDidTapPay() {
        presenter.didTapPay()
        
        XCTAssertTrue(router.navigateToQrPaymentResultCalled, "Router's navigateToQrPaymentResult should be called")
    }
    
    func testNavigateBack() {
        presenter.navigateBack()
        
        XCTAssertTrue(router.navigateBackCalled, "Router's navigateBackCalled should be called")
    }
}

class MockQrScannerView: QrScannerPresenterToView {
    var presenter: MobileApp.QrScannerViewToPresenter?
    
    var setupQrResultCalledWithNominal: String?
    var setupQrResultCalledWithTrxId: String?
    var setupQrResultCalledWithMerchant: String?
    var setupQrResultCalledWithSof: String?
    
    func setupQrResult(nominal: String, trxId: String, merchant: String, sof: String) {
        setupQrResultCalledWithNominal = nominal
        setupQrResultCalledWithTrxId = trxId
        setupQrResultCalledWithMerchant = merchant
        setupQrResultCalledWithSof = sof
    }
}

class MockQrScannerInteractor: QrScannerPresenterToInteractor {
    var presenter: MobileApp.QrScannerInteractorToPresenter?
}

class MockQrScannerRouter: QrScannerPresenterToRouter {
    var navigateToQrPaymentResultCalled = false
    var navigateBackCalled = false
    
    func navigateToQrPaymentResult(view: QrScannerPresenterToView?, context: QrViewModel.QrViewModel) {
        navigateToQrPaymentResultCalled = true
    }
    
    func navigateBack(from view: QrScannerPresenterToView?) {
        navigateBackCalled = true
    }
}
