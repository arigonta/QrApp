//
//  QrLandingPresenterTests.swift
//  MobileAppTests
//
//  Created by Armadi Gonta on 23/02/24.
//

import XCTest
@testable import MobileApp

class QrLandingPresenterTests: XCTestCase {
    
    var presenter: QrLandingPresenter!
    var view: MockQrLandingView!
    var interactor: MockQrLandingInteractor!
    var router: MockQrLandingRouter!

    override func setUpWithError() throws {
        view = MockQrLandingView()
        interactor = MockQrLandingInteractor()
        router = MockQrLandingRouter()
        presenter = QrLandingPresenter()
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
    }

    override func tearDownWithError() throws {
        view = nil
        interactor = nil
        router = nil
        presenter = nil
    }

    func testViewDidLoad() {
        presenter.viewDidLoad()
        XCTAssertEqual(view.setupViewCalledWithName, "Hi! Gonta", "View's setupView method should be called with correct name")
    }

    func testGetModel() {
        let model = presenter.getModel()
        XCTAssertEqual(model.name, "Gonta", "Model name should be 'Gonta'")
        XCTAssertEqual(model.nominal, "400000", "Model nominal should be '400000'")
    }

    func testDidTapQris() {
        presenter.didTapQris()
        XCTAssertTrue(router.navigateToQrScannerCalled, "Router's navigateToQrScanner should be called")
    }
    
    func testNavigateBack() {
        presenter.navigateBack()
        XCTAssertTrue(router.navigateBackCalled, "Router's navigateBackCalled should be called")
    }
}

// Mock classes for testing
class MockQrLandingView: QrLandingPresenterToView {
    var presenter: MobileApp.QrLandingViewToPresenter?
    
    var setupViewCalledWithName: String?
    var didNavigateBack: Bool = false
    
    func setupView(name: String, value: String) {
        setupViewCalledWithName = name
    }
    
    func navigateBack() {
        didNavigateBack = true
    }
}

class MockQrLandingInteractor: QrLandingPresenterToInteractor {
    var presenter: MobileApp.QrLandingInteractorToPresenter?
}

class MockQrLandingRouter: QrLandingPresenterToRouter {
    var navigateToQrScannerCalled = false
    var navigateBackCalled = false
    
    func navigateToQrScanner(view: QrLandingPresenterToView?, context: QrViewModel.QrViewModel) {
        navigateToQrScannerCalled = true
    }
    
    func navigateBack(from view: QrLandingPresenterToView?) {
        navigateBackCalled = true
    }
}
