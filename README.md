# QR App

QR App is a simple mobile application for scanning QR codes and performing related actions such as payments or accessing information associated with the QR code.

## Features

- **QR Code Scanner**: Scan QR codes using your device's camera.
- **Payment Integration**: Perform payments by scanning QR codes containing payment information.
- **Transaction History**: View a history of past transactions performed through the app.
- **Simple and Intuitive UI**: User-friendly interface for easy navigation and interaction.